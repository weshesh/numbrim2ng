﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numbrimäng
{
    class Program
    {
        static void Main(string[] args)
        {
            #region massiivideJaMuutujateKehtestamine
            int[] esimeneArv = { 1, 2, 3, 4, 5 };
            int[] teineArv = { 1, 2, 3, 4, 5 };
            int skoor = 0;
            int round = 1;
            int vastus = 0;
            #endregion
            #region logo
            Console.WriteLine();
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("--------________________nmMM-------------");
            Console.WriteLine("------zZMMMMMMMMMMMMMMMMMMM--------------");
            Console.WriteLine("------------MMM-----MMM------------------");
            Console.WriteLine("------------MMM-----MMM------------------");
            Console.WriteLine("------------dMM-----dMM------------------");
            Console.WriteLine("-------------MM------MM------------------");
            Console.WriteLine("------------np------np-------------------");
            Console.WriteLine("-----------------------------------------"); 
            #endregion
        algus:
            #region juhend
            Console.WriteLine("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
            Console.WriteLine("-Suur-matemaatikaalane-mõistatamise-mäng!");
            Console.WriteLine("------Korruta-numbreid-ja-võida-elu------");
            Console.WriteLine("mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm");
            Console.WriteLine(); 
            #endregion

            for (round = 1; round < 11; round++) //
            {
                #region ArvestuseTabel
                Console.WriteLine("---------------------");
                Console.WriteLine("Round:" + round);
                Console.WriteLine("Sinu skoor: " + skoor);
                Console.WriteLine("---------------------");
                #endregion
                #region randomNumberGenerator
                Random randE = new Random();
                Random randT = new Random();
                int currentEsimene = randE.Next(0, 5);
                int currentTeine = randT.Next(0, 5);
            #endregion

            askAgain:
                Console.WriteLine($"{esimeneArv[currentEsimene]} x {teineArv[currentTeine]} = ?"); //Ülesanne
                try
                { 
                vastus = int.Parse(Console.ReadLine());
                }
                catch (InvalidCastException)
                {
                    goto askAgain;
                }

                #region vastuseKontroll
                if (vastus == esimeneArv[currentEsimene] * teineArv[currentTeine])
                {
                    Console.WriteLine("Õige!");
                    skoor++;
                }
                else
                {
                    Console.WriteLine("Vale :'(");
                }
                #endregion
                Console.WriteLine("---------------------");
            }
            #region uusMäng
            Console.WriteLine("Uuesti? (Y/N)");
            string uuesti = Console.ReadLine();
            if (uuesti == "y" || uuesti == "yes")
            {
                goto algus;
            }
            else
            {
                Console.WriteLine("Ok. Tsau!");
            } 
            #endregion
        }
    }
}
